/**
 * @module orchestrate-js
 */

/**
 * @callback lifecycleFn
 * @return {(Promise<*>|*)}
 */

/**
 * @typedef {Object} ScriptTriggerSequence
 * @property {lifecycleFn} init
 * @property {lifecycleFn} finalize
 */

/**
 * @typedef {Object.<string, ScriptTriggerSequence>} ScriptOrchestrationDefinition
 */

/**
 * Orchestrates scripts based on CSS class attached to the <body> tag
 * 
 * @param {ScriptOrchestrationDefinition} scripts
 * @return {{fire: function, loadEvent: function}}}
 */
export default function (scripts) {
    var routeBasedJsOrchestrator = {
        fire: function (func, funcname, args) {
            var fire;
            funcname = (funcname === undefined) ? 'init' : funcname;
            fire = func !== '';
            fire = fire && scripts[func];
            fire = fire && typeof scripts[func][funcname] === 'function';

            if (fire) {
                return scripts[func][funcname](args);
            }
        },
        loadEvents: function () {
            return new Promise((resolve, reject) => {
                // cache for promises
                var proms = [];

                // Fire common init JS
                proms.push(Promise.resolve(routeBasedJsOrchestrator.fire('common', 'init')));

                // Fire page-specific init JS, and then finalize JS
                document.body.className.replace(/-/g, '_').split(/\s+/).forEach(function (className) {

                    var finalize = function () {
                        proms.push(Promise.resolve(routeBasedJsOrchestrator.fire(className, 'finalize')));                        
                    };
                    // await for init, then execute finalize even if init rejects
                    proms.push(Promise.resolve(routeBasedJsOrchestrator.fire(className, 'init')));
                    Promise.all(proms).then(finalize, finalize);
                });

                var finalizeCommon = function () {
                    proms.push(Promise.resolve(routeBasedJsOrchestrator.fire('common', 'finalize')));                    
                };

                // Fire common finalize JS, even if any previous promise rejected
                Promise.all(proms).then(finalizeCommon, finalizeCommon);

                // Resolve the current promise with the results of processing all callbacks (makes object testable)
                Promise.all(proms).then(() => {
                    resolve(proms);
                });
            });
        }
    };

    return routeBasedJsOrchestrator;
}
