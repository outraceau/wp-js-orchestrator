import orchestrate from '../index';
import $ from 'jquery';

describe("JS orchestrator - fire JS based on <body> CSS class and predictable async behaviour", function () {
    var fixture;
    var scripts;
    var promiseResolutionValues = [];
    
    beforeEach((done) => {
        document.body.classList.add(['latest-news']);

        scripts = {
            'common': { // Should run
                init: function () {
                    return new Promise((resolve, reject) => {
                        setTimeout(() => {
                            resolve('common:init');
                        }, 1000);     
                    });
               
                },
                finalize: function () {
                    return 'common:finalize';
                }
            },
            'latest_news': { // Should run
                init: function () {
                    return 'news:init';
                },
                finalize: function () {
                    return 'news:finalize';
                }
            },
            'discounted_products': { // Should not run
                init: function () {
                    return 'products:init';
                },
                finalize: function () {
                    return 'products:finalize';
                }
            }
        };
      
        orchestrate(scripts)
            .loadEvents()
            .then(promises => {
                promises.forEach(promise => { // Unpack promise resolution values for simple assertions
                    promise.then(value => {
                        promiseResolutionValues.push(value);
                    });
                });        
            })
            .then(done); // Run test caseds only when "done" is invoked
    });

    it("fires all qualified callbacks", function () {     
        expect(promiseResolutionValues).toEqual(jasmine.arrayContaining([
            'common:init',
            'common:finalize',
            'news:init',
            'news:finalize',
        ]));
    });

    it("does not fires unqualified callbacks", function () {     
        expect(promiseResolutionValues).not.toEqual(jasmine.arrayContaining([
            'products:init',
            'products:finalize',
        ]));
    });

    it("fires qualified callbacks in correct order", function () {     
        expect(promiseResolutionValues[0]).toEqual('common:init');
        expect(promiseResolutionValues[1]).toEqual('news:init');
        expect(promiseResolutionValues[2]).toEqual('news:finalize');
        expect(promiseResolutionValues[3]).toEqual('common:finalize');
    });
});
